package com.internship.rushhour.domain.provider.models;

import com.internship.rushhour.domain.employee.models.ProviderAdminRequestDto;
import org.hibernate.validator.constraints.URL;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;

public record ProviderCreateDTO(

        @NotBlank(message = "Name should not be blank")
        @Size(min = 3, message = "Length of a name must be at least 3 chars long")
        String name,

        @NotBlank(message = "WebSite should not be blank")
        @URL
        String webSite,

        @NotBlank(message = "businessDomain should not be blank")
        @Size(min = 2, message = "Length of a businessDomain must be at least 2 chars long")
        @Pattern(regexp = "^[A-Za-z]+$", message = "only letters are allowed")
        String businessDomain,

        @NotBlank(message = "Phone should not be blank")
        @Pattern(regexp = "^[+0-9]+$", message = "Numbers only , optionally can start with +")
        String phone,

        @NotNull(message = "startTime  should not be blank")
        LocalTime startTime,

        @NotNull(message = "endTime should not be blank")
        LocalTime endTime,

        @NotEmpty(message = "workingDays should not be blank")
        Set<DayOfWeek> workingDays,

        @NotNull(message = "ProviderAdminRequestDto should not be null")
        @Valid
        ProviderAdminRequestDto providerAdminRequestDto
) {
}
