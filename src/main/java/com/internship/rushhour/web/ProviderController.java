package com.internship.rushhour.web;

import com.internship.rushhour.domain.employee.service.EmployeeService;
import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.domain.provider.mappers.ProviderMapper;
import com.internship.rushhour.domain.provider.models.ProviderCreateDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;
import com.internship.rushhour.domain.provider.service.ProviderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/providers")
public class ProviderController {
    private final ProviderService providerService;

    private final ProviderMapper providerMapper;
    private final EmployeeService employeeService;

    @Autowired
    public ProviderController(ProviderService providerService, ProviderMapper providerMapper, EmployeeService employeeService) {
        this.providerService = providerService;
        this.providerMapper = providerMapper;
        this.employeeService = employeeService;
    }

    @PostMapping
    @Operation(summary = "Create a provider administrator")
    public ResponseEntity<ProviderGetDTO> create(@Valid @RequestBody ProviderCreateDTO providerCreateDTO) {
        Provider provider = providerService.create(providerCreateDTO);
        employeeService.createProviderAdmin(providerCreateDTO.providerAdminRequestDto(), provider);
        return ResponseEntity.ok(providerMapper.providerToProviderGetDTO(provider));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a provider with choosing id")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROVIDER_ADMIN') && @permissionService.canProviderAdminManageProvider(#id)")
    public ResponseEntity<ProviderGetDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(providerService.getById(id));
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('CLIENT')")
    public ResponseEntity<Page<ProviderGetDTO>> get(Pageable pageable) {
        return ResponseEntity.ok(providerService.get(pageable));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a provider")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        providerService.delete(id);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Updating Provider Information")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROVIDER_ADMIN') && @permissionService.canProviderAdminManageProvider(#id)")
    public ResponseEntity<ProviderGetDTO> update(@Valid @RequestBody ProviderCreateDTO providerCreateDTO,
                                                 @PathVariable Long id) {
        return ResponseEntity.ok(providerService.update(providerCreateDTO, id));
    }
}
