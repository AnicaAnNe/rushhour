CREATE TABLE IF NOT EXISTS employees(
   id int NOT NULL AUTO_INCREMENT,
   title varchar(255) NOT NULL,
   phone varchar(255) NOT NULL,
   rate_per_hour double NOT NULL,
   hire_date DATE NOT NULL,
   providers_id int NOT NULL,
   account_id int NOT NULL,
   PRIMARY KEY(id),
   FOREIGN KEY (providers_id) REFERENCES providers(id),
   FOREIGN KEY (account_id) REFERENCES accounts(id)
);


