package com.internship.rushhour.domain.account.service;

import com.internship.rushhour.domain.account.models.AccountGetDTO;


public interface AccountService {

    AccountGetDTO getById(Long id);

    void checkIfEmailExists(String email);

}
