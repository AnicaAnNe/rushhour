package com.internship.rushhour.domain.client.service;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.account.models.AccountCreateDTO;
import com.internship.rushhour.domain.account.models.AccountGetDTO;
import com.internship.rushhour.domain.account.service.AccountServiceImpl;
import com.internship.rushhour.domain.client.entity.Client;
import com.internship.rushhour.domain.client.mappers.ClientMapper;
import com.internship.rushhour.domain.client.models.ClientCreateDTO;
import com.internship.rushhour.domain.client.models.ClientGetDTO;
import com.internship.rushhour.domain.client.repository.ClientRepository;
import com.internship.rushhour.domain.role.models.RoleGetDTO;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {
    @Mock
    ClientRepository clientRepository;

    @Mock
    ClientMapper clientMapper;

    @Mock
    AccountServiceImpl accountService;

    @InjectMocks
    ClientServiceImpl clientService;


    @Test
    void createTestValid() {

        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_CLIENT");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        ClientCreateDTO clientCreatedDTO = new ClientCreateDTO("6456456", "bulevar", accountCreateDTO);
        Client client = new Client(1L, "6456456", "bulevar", account);
        ClientGetDTO clientGetDTO = new ClientGetDTO(1L, "46546546", "bulevar", accountGetDTO);


        doNothing().when(accountService).checkIfEmailExists("anica@gmail.com");
        when(clientMapper.clientCreatedDTOtoClient(clientCreatedDTO)).thenReturn(client);
        when(clientRepository.save(client)).thenReturn(client);
        when(clientMapper.clientToClientGetDTO(client)).thenReturn(clientGetDTO);
        assertEquals(clientGetDTO, clientService.create(clientCreatedDTO));
    }

    @Test
    void createTestInValidAccountEmail() {

        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_CLIENT");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        ClientCreateDTO clientCreatedDTO = new ClientCreateDTO("6456456", "bulevar", accountCreateDTO);
        Client client = new Client(1L, "6456456", "bulevar", account);
        ClientGetDTO clientGetDTO = new ClientGetDTO(1L, "46546546", "bulevar", accountGetDTO);

        doThrow(ConflictException.class).when(accountService).checkIfEmailExists("anica@gmail.com");
        assertThrows(ConflictException.class, () -> clientService.create(clientCreatedDTO));
    }

    @Test
    void deleteTestValid() {
        when(clientRepository.existsById(1L)).thenReturn(true);
        clientService.delete(1L);
        verify(clientRepository, times(1)).deleteById(1L);
    }

    @Test
    void deleteTestInvalid() {
        when(clientRepository.existsById(4L)).thenReturn(false);
        assertThrows(EntityMissingException.class, () -> clientService.delete(4L));
    }

    @Test
    void getByIdTestValid() {
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_CLIENT");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        ClientCreateDTO clientCreatedDTO = new ClientCreateDTO("6456456", "bulevar", accountCreateDTO);
        Client client = new Client(1L, "6456456", "bulevar", account);
        ClientGetDTO clientGetDTO = new ClientGetDTO(1L, "46546546", "bulevar", accountGetDTO);

        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        when(clientMapper.clientToClientGetDTO(client)).thenReturn(clientGetDTO);
        assertEquals(clientGetDTO, clientService.getById(1L));
    }

    @Test
    void getByIdTestInvalid() {
        when(clientRepository.findById(5L)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> clientService.getById(5L));
    }

    @Test
    void pageableTest() {
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_CLIENT");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        ClientCreateDTO clientCreatedDTO = new ClientCreateDTO("6456456", "bulevar", accountCreateDTO);
        Client client = new Client(1L, "6456456", "bulevar", account);
        ClientGetDTO clientGetDTO = new ClientGetDTO(1L, "46546546", "bulevar", accountGetDTO);

        Page<ClientGetDTO> clients;
        List<ClientGetDTO> clientList = new ArrayList<>();
        clientList.add(clientGetDTO);
        clients = new PageImpl<>(clientList);

        Pageable pageable = Mockito.mock(Pageable.class);

        Page<Client> clientPage = new PageImpl<>(List.of(client));
        when(clientRepository.findAll(pageable)).thenReturn(clientPage);

        when(clientMapper.clientToClientGetDTO(client)).thenReturn(clientGetDTO);
        Page<ClientGetDTO> result = clientService.get(pageable);

        assertEquals(result.getContent().get(0), clientGetDTO);
        assertEquals(result.getSize(), 1);
    }

    @Test
    void updateTestValid() {
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_CLIENT");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        ClientCreateDTO clientCreatedDTO = new ClientCreateDTO("6456456", "bulevar", accountCreateDTO);
        Client client = new Client(1L, "6456456", "bulevar", account);
        ClientGetDTO clientGetDTO = new ClientGetDTO(1L, "46546546", "bulevar", accountGetDTO);

        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        when(clientRepository.save(client)).thenReturn(client);
        when(clientMapper.clientToClientGetDTO(client)).thenReturn(clientGetDTO);

        assertEquals(clientGetDTO, clientService.update(clientCreatedDTO, 1L));
    }

    @Test
    void updateTestInvalidProviderMissing() {
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        ClientCreateDTO clientCreatedDTO = new ClientCreateDTO("6456456", "bulevar", accountCreateDTO);

        when(clientRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> clientService.update(clientCreatedDTO, 1L));
    }


}
