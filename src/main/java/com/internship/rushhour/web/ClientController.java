package com.internship.rushhour.web;

import com.internship.rushhour.domain.client.models.ClientCreateDTO;
import com.internship.rushhour.domain.client.models.ClientGetDTO;
import com.internship.rushhour.domain.client.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/clients")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    @Operation(summary = "Create a client")
    public ResponseEntity<ClientGetDTO> create(@Valid @RequestBody ClientCreateDTO clientCreatedDTO) {
        return ResponseEntity.ok(clientService.create(clientCreatedDTO));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a client with choosing  id")
    @PreAuthorize("hasRole('ADMIN') || hasRole('CLIENT') && @permissionService.canClientAccess(#id)")
    public ResponseEntity<ClientGetDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(clientService.getById(id));
    }

    @GetMapping
    public ResponseEntity<Page<ClientGetDTO>> get(Pageable pageable) {
        return ResponseEntity.ok(clientService.get(pageable));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a client with choosing  id")
    @PreAuthorize("hasRole('ADMIN') || hasRole('CLIENT') && @permissionService.canClientAccess(#id)")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        clientService.delete(id);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a client with choosing  id")
    @PreAuthorize("hasRole('ADMIN') || hasRole('CLIENT') && @permissionService.canClientAccess(#id)")
    public ResponseEntity<ClientGetDTO> update(@Valid @RequestBody ClientCreateDTO clientCreatedDTO,
                                               @PathVariable Long id) {
        return ResponseEntity.ok(clientService.update(clientCreatedDTO, id));
    }

}
