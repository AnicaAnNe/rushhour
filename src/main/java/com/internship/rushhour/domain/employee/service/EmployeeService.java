package com.internship.rushhour.domain.employee.service;

import com.internship.rushhour.domain.employee.entity.Employee;
import com.internship.rushhour.domain.employee.models.EmployeeCreateDTO;
import com.internship.rushhour.domain.employee.models.EmployeeGetDTO;
import com.internship.rushhour.domain.employee.models.ProviderAdminRequestDto;
import com.internship.rushhour.domain.provider.entity.Provider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeService {

    EmployeeGetDTO create(EmployeeCreateDTO employeeCreateDTO);

    EmployeeGetDTO getById(Long id);

    Page<EmployeeGetDTO> get(Pageable pageable);

    void delete(Long id);

    EmployeeGetDTO update(EmployeeCreateDTO employeeCreateDTO, Long id);

    void createProviderAdmin(ProviderAdminRequestDto providerAdminRequestDto, Provider provider);

    Employee findByAccountId(Long id);

    List<Employee> findByProviderId(Long id);

}
