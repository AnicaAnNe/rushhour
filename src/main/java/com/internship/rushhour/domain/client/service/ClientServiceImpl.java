package com.internship.rushhour.domain.client.service;

import com.internship.rushhour.domain.account.service.AccountService;
import com.internship.rushhour.domain.client.entity.Client;
import com.internship.rushhour.domain.client.mappers.ClientMapper;
import com.internship.rushhour.domain.client.models.ClientCreateDTO;
import com.internship.rushhour.domain.client.models.ClientGetDTO;
import com.internship.rushhour.domain.client.repository.ClientRepository;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;

    private final ClientMapper clientMapper;

    private final AccountService accountService;

    public ClientServiceImpl(ClientRepository clientRepository, ClientMapper clientMapper, AccountService accountService) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.accountService = accountService;
    }

    @Override
    public ClientGetDTO create(ClientCreateDTO clientCreatedDTO) {
        accountService.checkIfEmailExists(clientCreatedDTO.account().email());
        Client client = clientMapper.clientCreatedDTOtoClient(clientCreatedDTO);
        return clientMapper.clientToClientGetDTO(clientRepository.save(client));
    }

    @Override
    public ClientGetDTO getById(Long id) {
        Client client = getEntityById(id);
        return clientMapper.clientToClientGetDTO(client);
    }

    @Override
    public Page<ClientGetDTO> get(Pageable pageable) {
        return clientRepository.findAll(pageable).map(clientMapper::clientToClientGetDTO);
    }

    @Override
    public void delete(Long id) {
        if (!clientRepository.existsById(id)) {
            throw new EntityMissingException("That Client does not exist with id " + id);
        }
        clientRepository.deleteById(id);
    }

    @Override
    public ClientGetDTO update(ClientCreateDTO clientCreatedDTO, Long id) {
        Client client = getEntityById(id);

        clientMapper.update(client, clientCreatedDTO);
        return clientMapper.clientToClientGetDTO(clientRepository.save(client));
    }

    private Client getEntityById(Long id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new EntityMissingException("That Client does not exists with id " + id));
    }
}
