package com.internship.rushhour.web;

import com.internship.rushhour.domain.employee.models.EmployeeCreateDTO;
import com.internship.rushhour.domain.employee.models.EmployeeGetDTO;
import com.internship.rushhour.domain.employee.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    @Operation(summary = "Create an employee")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROVIDER_ADMIN') && @permissionService.canProviderAdminCreateEmployee(#employeeCreateDTO)")
    public ResponseEntity<EmployeeGetDTO> create(@Valid @RequestBody EmployeeCreateDTO employeeCreateDTO) {
        return ResponseEntity.ok(employeeService.create(employeeCreateDTO));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get an employee with choosing  id")
    @PreAuthorize("hasRole('ADMIN') || (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccess(#id))" +
            "|| (hasRole('PROVIDER_ADMIN') && @permissionService.canProviderAdminManageEmployee(#id))")
    public ResponseEntity<EmployeeGetDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(employeeService.getById(id));
    }

    @GetMapping
    public ResponseEntity<Page<EmployeeGetDTO>> get(Pageable pageable) {
        return ResponseEntity.ok(employeeService.get(pageable));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete an employee")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROVIDER_ADMIN') && @permissionService.canProviderAdminManageEmployee(#id)")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        employeeService.delete(id);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update an employee")
    @PreAuthorize("hasRole('ADMIN') || (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccess(#id))" +
            "|| (hasRole('PROVIDER_ADMIN') && @permissionService.canProviderAdminManageEmployee(#id))")
    public ResponseEntity<EmployeeGetDTO> update(@Valid @RequestBody EmployeeCreateDTO employeeCreateDTO,
                                                 @PathVariable Long id) {
        return ResponseEntity.ok(employeeService.update(employeeCreateDTO, id));
    }

}
