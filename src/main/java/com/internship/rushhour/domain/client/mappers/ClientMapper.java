package com.internship.rushhour.domain.client.mappers;

import com.internship.rushhour.domain.account.mappers.AccountMapper;
import com.internship.rushhour.domain.account.service.AccountService;
import com.internship.rushhour.domain.client.entity.Client;
import com.internship.rushhour.domain.client.models.ClientCreateDTO;
import com.internship.rushhour.domain.client.models.ClientGetDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {AccountService.class, AccountMapper.class})
public interface ClientMapper {
    @Mapping(source = "account", target = "account", qualifiedByName = "clientAccount")
    Client clientCreatedDTOtoClient(ClientCreateDTO clientCreatedDTO);

    @Mapping(source = "account.id", target = "accountGetDTO")
    ClientGetDTO clientToClientGetDTO(Client client);

    @Mapping(source = "account", target = "account", qualifiedByName = "updateAccount")
    void update(@MappingTarget Client client, ClientCreateDTO clientCreatedDTO);
}
