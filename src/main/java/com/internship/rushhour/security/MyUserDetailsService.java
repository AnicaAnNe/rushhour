package com.internship.rushhour.security;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.account.repository.AccountRepository;
import com.internship.rushhour.security.models.ContextUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class MyUserDetailsService implements UserDetailsService {


    @Autowired
    private AccountRepository accountRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Account account = accountRepository.findByEmail(username);

        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        return new ContextUser(account.getEmail(), account.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority(account.getRole().getName())), account.getId());
    }
}
