package com.internship.rushhour.domain.account.models;
import com.internship.rushhour.domain.role.models.RoleGetDTO;

public record AccountGetDTO(Long id, String fullName, String email, RoleGetDTO role) {
}
