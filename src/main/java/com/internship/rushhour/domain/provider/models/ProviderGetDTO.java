package com.internship.rushhour.domain.provider.models;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;


public record ProviderGetDTO(Long id, String name, String webSite, String businessDomain, String phone,
                             LocalTime startTime,
                             LocalTime endTime, Set<DayOfWeek> workingDays) {}
