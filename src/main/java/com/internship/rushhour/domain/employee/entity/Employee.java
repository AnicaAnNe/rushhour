package com.internship.rushhour.domain.employee.entity;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.provider.entity.Provider;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity(name = "employees")
public class Employee {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @NotBlank(message = "Title should not be blank")
        @Size(min = 2, message = "Length of a title must be at least 2 chars long")
        private String title;

        @NotBlank(message = "Phone should not be blank")
        @Pattern(regexp = "^[+0-9]+$" , message = "Numbers only , optionally can start with +")
        private String phone;

        @NotNull(message = "ratePerHour should not be blank")
        @Column(name = "rate_per_hour")
        @Min(value = 0)
        private double ratePerHour;

        @NotNull(message = "hireDate should not be blank")
        @Column(name = "hire_date")
        private LocalDate hireDate;

        @ManyToOne
        @JoinColumn(name = "providers_id")
        private Provider provider;

        @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
        @JoinColumn(name = "account_id")
        private Account account;

        public Employee() {}

        public Employee(Long id, String title, String phone, Long ratePerHour, LocalDate hireDate,
                        Provider provider, Account account) {
                this.id = id;
                title = title;
                this.phone = phone;
                this.ratePerHour = ratePerHour;
                this.hireDate = hireDate;
                this.provider = provider;
                this.account = account;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public String getPhone() {
                return phone;
        }

        public void setPhone(String phone) {
                this.phone = phone;
        }

        public double getRatePerHour() {
                return ratePerHour;
        }

        public void setRatePerHour(Long ratePerHour) {
                this.ratePerHour = ratePerHour;
        }

        public LocalDate getHireDate() {
                return hireDate;
        }

        public void setHireDate(LocalDate hireDate) {
                this.hireDate = hireDate;
        }

        public Provider getProvider() {
                return provider;
        }

        public void setProvider(Provider provider) {
                this.provider = provider;
        }

        public Account getAccount() {
                return account;
        }

        public void setAccount(Account account) {
                this.account = account;
        }
}
