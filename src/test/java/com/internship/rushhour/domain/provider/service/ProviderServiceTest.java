package com.internship.rushhour.domain.provider.service;

import com.internship.rushhour.domain.account.models.AccountCreateDTO;
import com.internship.rushhour.domain.employee.models.ProviderAdminRequestDto;
import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.domain.provider.mappers.ProviderMapper;
import com.internship.rushhour.domain.provider.models.ProviderCreateDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;
import com.internship.rushhour.domain.provider.repository.ProviderRepository;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProviderServiceTest {

    @Mock
    ProviderRepository providerRepository;

    @Mock
    ProviderMapper providerMapper;

    @InjectMocks
    ProviderServiceImpl providerService;

    @Test
    void createTestValid() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        when(providerRepository.existsByName("instagram123")).thenReturn(false);
        when(providerRepository.existsByBusinessDomain("instagram")).thenReturn(false);
        when(providerMapper.providerCreateDTOtoProvider(providerCreateDTO)).thenReturn(provider);
        when(providerRepository.save(provider)).thenReturn(provider);
        assertEquals(provider, providerService.create(providerCreateDTO));
    }

    @Test
    void createTestInValidCreatingProviderWithExistingName() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        when(providerRepository.existsByName("instagram123")).thenReturn(true);
        assertThrows(ConflictException.class, () -> providerService.create(providerCreateDTO));
    }

    @Test
    void createTestInValidCreatingProviderWithExistingBusinessDomain() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        when(providerRepository.existsByName("instagram123")).thenReturn(false);
        when(providerRepository.existsByBusinessDomain("instagram")).thenReturn(true);
        assertThrows(ConflictException.class, () -> providerService.create(providerCreateDTO));
    }

    @Test
    void updateTestValid() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);


        when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));
        when(providerRepository.save(provider)).thenReturn(provider);
        when(providerMapper.providerToProviderGetDTO(provider)).thenReturn(providerGetDTO);

        assertEquals(providerGetDTO, providerService.update(providerCreateDTO, 1L));
    }

    @Test
    void updateTestInvalidProviderMissing() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        when(providerRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> providerService.update(providerCreateDTO, 1L));
    }

    @Test
    void updateTestInvalidChangingName() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("insta",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));
        assertThrows(ConflictException.class, () -> providerService.update(providerCreateDTO, 1L));
    }

    @Test
    void updateTestInvalidChangingBusinessDomain() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "insta", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));
        assertThrows(ConflictException.class, () -> providerService.update(providerCreateDTO, 1L));
    }

    @Test
    void deleteTestValid() {
        when(providerRepository.existsById(1L)).thenReturn(true);
        providerService.delete(1L);
        verify(providerRepository, times(1)).deleteById(1L);

    }

    @Test
    void deleteTestInvalid() {
        when(providerRepository.existsById(4L)).thenReturn(false);
        assertThrows(EntityMissingException.class, () -> providerService.delete(4L));
    }

    @Test
    void getByIdTestValid() {
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);

        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));
        when(providerMapper.providerToProviderGetDTO(provider)).thenReturn(providerGetDTO);
        assertEquals(providerGetDTO, providerService.getById(1L));
    }

    @Test
    void getByIdTestInvalid() {
        when(providerRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> providerService.getById(1L));
    }

    @Test
    void pageableTest() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);

        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));

        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        Page<ProviderGetDTO> providers;
        List<ProviderGetDTO> providerList = new ArrayList<>();
        providerList.add(providerGetDTO);
        providers = new PageImpl<>(providerList);

        Pageable pageable = Mockito.mock(Pageable.class);

        Page<Provider> providerPage = new PageImpl<>(List.of(provider));
        when(providerRepository.findAll(pageable)).thenReturn(providerPage);

        when(providerMapper.providerToProviderGetDTO(provider)).thenReturn(providerGetDTO);
        Page<ProviderGetDTO> result = providerService.get(pageable);

        assertEquals(result.getContent().get(0), providerGetDTO);
        assertEquals(result.getSize(), 1);
    }


}
