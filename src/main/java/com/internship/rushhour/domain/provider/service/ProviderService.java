package com.internship.rushhour.domain.provider.service;

import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.domain.provider.models.ProviderCreateDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProviderService {

    Provider create(ProviderCreateDTO providerCreateDTO);

    ProviderGetDTO getById(Long id);

    Page<ProviderGetDTO> get(Pageable pageable);

    void delete(Long id);

    ProviderGetDTO update(ProviderCreateDTO providerCreateDTO, Long id);

    Provider getEntityById(Long id);

}