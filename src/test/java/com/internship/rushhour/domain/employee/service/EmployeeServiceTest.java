package com.internship.rushhour.domain.employee.service;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.account.models.AccountCreateDTO;
import com.internship.rushhour.domain.account.models.AccountGetDTO;
import com.internship.rushhour.domain.account.service.AccountServiceImpl;
import com.internship.rushhour.domain.employee.entity.Employee;
import com.internship.rushhour.domain.employee.mappers.EmployeeMapper;
import com.internship.rushhour.domain.employee.models.EmployeeCreateDTO;
import com.internship.rushhour.domain.employee.models.EmployeeGetDTO;
import com.internship.rushhour.domain.employee.models.ProviderAdminRequestDto;
import com.internship.rushhour.domain.employee.repository.EmployeeRepository;
import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.domain.provider.models.ProviderCreateDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;
import com.internship.rushhour.domain.role.models.RoleGetDTO;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    EmployeeMapper employeeMapper;

    @Mock
    AccountServiceImpl accountService;
    @InjectMocks
    EmployeeServiceImpl employeeService;


    @Test
    void createTestValid() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));
        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);
        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_EMPLOYEE");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO("window", "55464654", 120L,
                date, accountCreateDTO, 1L);
        Employee employee = new Employee(1L, "window", "55464654", 120L,
                date, provider, account);
        EmployeeGetDTO employeeGetDTO = new EmployeeGetDTO("window", "55464654", 120L,
                date, providerGetDTO, accountGetDTO);

        doNothing().when(accountService).checkIfEmailExists("anica@gmail.com");
        when(employeeMapper.employeeCreateDTOtoEmployee(employeeCreateDTO)).thenReturn(employee);
        when(employeeRepository.save(employee)).thenReturn(employee);
        when(employeeMapper.employeeToEmployeeGetDTO(employee)).thenReturn(employeeGetDTO);
        assertEquals(employeeGetDTO, employeeService.create(employeeCreateDTO));
    }

    @Test
    void createTestInValidAccountEmail() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));
        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);
        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_EMPLOYEE");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO("window", "55464654", 120L,
                date, accountCreateDTO, 1L);
        Employee employee = new Employee(1L, "window", "55464654", 120L,
                date, provider, account);
        EmployeeGetDTO employeeGetDTO = new EmployeeGetDTO("window", "55464654", 120L,
                date, providerGetDTO, accountGetDTO);

        doThrow(ConflictException.class).when(accountService).checkIfEmailExists("anica@gmail.com");
        assertThrows(ConflictException.class, () -> employeeService.create(employeeCreateDTO));
    }

    @Test
    void deleteTestValid() {
        when(employeeRepository.existsById(1L)).thenReturn(true);
        employeeService.delete(1L);
        verify(employeeRepository, times(1)).deleteById(1L);
    }

    @Test
    void deleteTestInvalid() {
        when(employeeRepository.existsById(4L)).thenReturn(false);
        assertThrows(EntityMissingException.class, () -> employeeService.delete(4L));
    }

    @Test
    void getByIdTestValid() {
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_EMPLOYEE");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);
        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        LocalDate date = LocalDate.of(2023, 1, 23);
        Employee employee = new Employee(1L, "window", "55464654", 120L,
                date, provider, account);
        EmployeeGetDTO employeeGetDTO = new EmployeeGetDTO("window", "55464654", 120L,
                date, providerGetDTO, accountGetDTO);

        when(employeeRepository.findById(1L)).thenReturn(Optional.of(employee));
        when(employeeMapper.employeeToEmployeeGetDTO(employee)).thenReturn(employeeGetDTO);
        assertEquals(employeeGetDTO, employeeService.getById(1L));
    }

    @Test
    void getByIdTestInvalid() {
        when(employeeRepository.findById(5L)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> employeeService.getById(5L));
    }

    @Test
    void pageableTest() {
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_EMPLOYEE");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);
        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        LocalDate date = LocalDate.of(2023, 1, 23);
        Employee employee = new Employee(1L, "window", "55464654", 120L,
                date, provider, account);
        EmployeeGetDTO employeeGetDTO = new EmployeeGetDTO("window", "55464654", 120L,
                date, providerGetDTO, accountGetDTO);

        Page<EmployeeGetDTO> employees;
        List<EmployeeGetDTO> employeeList = new ArrayList<>();
        employeeList.add(employeeGetDTO);
        employees = new PageImpl<>(employeeList);

        Pageable pageable = Mockito.mock(Pageable.class);

        Page<Employee> employeePage = new PageImpl<>(List.of(employee));
        when(employeeRepository.findAll(pageable)).thenReturn(employeePage);

        when(employeeMapper.employeeToEmployeeGetDTO(employee)).thenReturn(employeeGetDTO);
        Page<EmployeeGetDTO> result = employeeService.get(pageable);

        assertEquals(result.getContent().get(0), employeeGetDTO);
        assertEquals(result.getSize(), 1);
    }

    @Test
    void updateTestValid() {
        LocalDate date = LocalDate.of(2023, 1, 23);
        Set<DayOfWeek> workingDays = new HashSet<>();
        LocalTime start = LocalTime.of(2, 1, 50, 30);
        LocalTime end = LocalTime.of(5, 1, 5, 50);
        ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO("instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays,
                new ProviderAdminRequestDto("window", "55464654", 120L,
                        date, new AccountCreateDTO("anica@gmail.com", "anica",
                        "pass")));
        ProviderGetDTO providerGetDTO = new ProviderGetDTO(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);
        Provider provider = new Provider(1L, "instagram123",
                "www.instagram.com", "instagram", "586", start, end, workingDays);

        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_EMPLOYEE");
        Account account = new Account(1L, "anica@gmail.com", "anica", "ajldkjsa");
        AccountGetDTO accountGetDTO = new AccountGetDTO(1L, "anica", "anica@gmail.com", role);

        EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO("window", "55464654", 120L,
                date, accountCreateDTO, 1L);
        Employee employee = new Employee(1L, "window", "55464654", 120L,
                date, provider, account);
        EmployeeGetDTO employeeGetDTO = new EmployeeGetDTO("window", "55464654", 120L,
                date, providerGetDTO, accountGetDTO);


        when(employeeRepository.findById(1L)).thenReturn(Optional.of(employee));
        when(employeeRepository.save(employee)).thenReturn(employee);
        when(employeeMapper.employeeToEmployeeGetDTO(employee)).thenReturn(employeeGetDTO);

        assertEquals(employeeGetDTO, employeeService.update(employeeCreateDTO, 1L));
    }

    @Test
    void updateTestInvalidProviderMissing() {
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO("anica@gmail.com", "anica",
                "pass");
        LocalDate date = LocalDate.of(2023, 1, 23);
        EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO("window", "55464654", 120L,
                date, accountCreateDTO, 1L);

        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> employeeService.update(employeeCreateDTO, 1L));
    }




}
