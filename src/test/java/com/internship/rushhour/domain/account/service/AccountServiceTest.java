package com.internship.rushhour.domain.account.service;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.account.mappers.AccountMapper;
import com.internship.rushhour.domain.account.models.AccountGetDTO;
import com.internship.rushhour.domain.account.repository.AccountRepository;
import com.internship.rushhour.domain.role.models.RoleGetDTO;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    AccountRepository accountRepository;
    @Mock
    AccountMapper accountMapper;
    @InjectMocks
    AccountServiceImpl accountService;

    Long id;

    @Test
    void getByIdTestValid() {
        id = 7L;
        RoleGetDTO role = new RoleGetDTO(1L, "ROLE_ADMIN");
        Account account = new Account(id, "anica@gmail.com", "anica", "pass");
        AccountGetDTO accountGetDTO = new AccountGetDTO(15L, "anica", "anica@gmail.com",
                role);

        when(accountRepository.findById(id)).thenReturn(Optional.of(account));
        when(accountMapper.accountToAccountGetDTO(account)).thenReturn(accountGetDTO);
        assertEquals(accountGetDTO, accountService.getById(id));
    }

    @Test
    void getByIdTestInvalid() {
        id = 200L;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(EntityMissingException.class, () -> accountService.getById(id));

    }

    @Test
    void checkIfEmailExistsValid() {
        String email = "anica@gmail.com";
        when(accountRepository.existsByEmail(email)).thenReturn(true);
        assertThrows(ConflictException.class, () -> accountService.checkIfEmailExists(email));
    }

    @Test
    void checkIfEmailExistsInvalid() {
        String email = "anica@gmail.com";
        when(accountRepository.existsByEmail(email)).thenReturn(false);
        accountService.checkIfEmailExists(email);
        verify(accountRepository, times(1)).existsByEmail(email);
    }


}
