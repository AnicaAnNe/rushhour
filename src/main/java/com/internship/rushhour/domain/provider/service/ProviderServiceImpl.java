package com.internship.rushhour.domain.provider.service;

import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.domain.provider.mappers.ProviderMapper;
import com.internship.rushhour.domain.provider.models.ProviderCreateDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;
import com.internship.rushhour.domain.provider.repository.ProviderRepository;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProviderServiceImpl implements ProviderService {
    private final ProviderRepository providerRepository;
    private final ProviderMapper providerMapper;

    @Autowired
    public ProviderServiceImpl(ProviderRepository providerRepository, ProviderMapper providerMapper) {
        this.providerRepository = providerRepository;
        this.providerMapper = providerMapper;
    }

    @Override
    public Provider create(ProviderCreateDTO providerCreateDTO) {
        if (providerRepository.existsByName(providerCreateDTO.name())) {
            throw new ConflictException("That Name is already exists , try with another");
        }
        if (providerRepository.existsByBusinessDomain(providerCreateDTO.businessDomain())) {
            throw new ConflictException("That Business Domain is already exists , try with another");
        }
        return providerRepository.save(providerMapper.providerCreateDTOtoProvider(providerCreateDTO));
    }

    @Override
    public ProviderGetDTO getById(Long id) {
        Provider provider = getEntityById(id);
        return providerMapper.providerToProviderGetDTO(provider);
    }

    @Override
    public Page<ProviderGetDTO> get(Pageable pageable) {
        return providerRepository.findAll(pageable).map(providerMapper::providerToProviderGetDTO);
    }

    @Override
    public void delete(Long id) {
        if (!providerRepository.existsById(id))
            throw new EntityMissingException("That Provider does not exists with id " + id);
        providerRepository.deleteById(id);
    }

    @Override
    public ProviderGetDTO update(ProviderCreateDTO providerCreateDTO, Long id) {
        Provider provider = getEntityById(id);

        if (!(providerCreateDTO.name().equals(provider.getName()))) {
            throw new ConflictException("You can not change name!");
        }
        if (!(providerCreateDTO.businessDomain().equals(provider.getBusinessDomain()))) {
            throw new ConflictException("You can not change businessDomain!");
        }
        providerMapper.update(provider, providerCreateDTO);
        return providerMapper.providerToProviderGetDTO(providerRepository.save(provider));
    }

    @Override
    public Provider getEntityById(Long id) {
        return providerRepository.findById(id)
                .orElseThrow(() -> new EntityMissingException("That Provider does not exists with id " + id));
    }

}
