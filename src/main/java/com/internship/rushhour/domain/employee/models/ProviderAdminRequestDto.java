package com.internship.rushhour.domain.employee.models;

import com.internship.rushhour.domain.account.models.AccountCreateDTO;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;

public record ProviderAdminRequestDto(
        @NotBlank(message = "Title should not be blank")
        @Size(min = 2, message = "Length of a title must be at least 2 chars long")
        String title,

        @NotBlank(message = "Phone should not be blank")
        @Pattern(regexp = "^[+0-9]+$" , message = "Numbers only , optionally can start with +")
        String phone,

        @NotNull(message = "ratePerHour should not be blank")
        @Min(value = 0)
        double ratePerHour,

        @NotNull(message = "hireDate should not be blank")
        LocalDate hireDate,

        @NotNull(message = "Account should not be null")
        @Valid
        AccountCreateDTO account
) {
}
