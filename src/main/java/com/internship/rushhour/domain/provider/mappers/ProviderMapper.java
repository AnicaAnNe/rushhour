package com.internship.rushhour.domain.provider.mappers;

import com.internship.rushhour.domain.employee.mappers.EmployeeMapper;
import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.domain.provider.models.ProviderCreateDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProviderMapper {


    Provider providerCreateDTOtoProvider(ProviderCreateDTO providerCreateDTO);

    ProviderGetDTO providerToProviderGetDTO(Provider provider);

    void update(@MappingTarget Provider pr, ProviderCreateDTO providerCreateDTO);
}
