package com.internship.rushhour.domain.role.service;

import com.internship.rushhour.domain.role.entity.Role;


public interface RoleService {

    Role getByName(String name);
}
