package com.internship.rushhour.domain.client.models;

import com.internship.rushhour.domain.account.models.AccountCreateDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public record ClientCreateDTO(

        @NotBlank(message = "Phone should not be blank")
        @Pattern(regexp = "^[+0-9]+$" , message = "Numbers only , optionally can start with +")
        String phone,

        @NotBlank(message = "Address should not be blank")
        @Size(min = 3, message = "Length of a address must be at least 3 chars long")
        String address,

        @NotNull(message = "Account should not be null")
        @Valid
        AccountCreateDTO account ){}
