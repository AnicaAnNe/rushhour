package com.internship.rushhour.domain.provider.entity;

import com.internship.rushhour.domain.employee.entity.Employee;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "providers")
public class Provider
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name should not be blank")
    @Column(unique = true)
    @Size(min = 3, message = "Length of a name must be at least 3 chars long")
    private String name;

    @NotBlank(message = "WebSite should not be blank")
    @URL
    private String webSite;

    @NotBlank(message = "businessDomain should not be blank")
    @Column(unique = true, name="business_domain")
    @Size(min = 2, message = "Length of a businessDomain must be at least 2 chars long")
    @Pattern(regexp = "^[A-Za-z]+$",
            message = "only letters are allowed")
    private String businessDomain;

    @NotBlank(message = "Phone should not be blank")
    @Pattern(regexp = "^[+0-9]+$" , message = "Numbers only , optionally can start with +")
    private String phone;

    @NotNull(message = "startTime should not be blank")
    @Column(name="start_time")
    private LocalTime startTime;

    @NotNull(message = "endTime should not be blank")
    @Column(name="end_time")
    private LocalTime endTime;

    @Column(name="working_days")
    @NotEmpty(message = "workingDays should not be blank")
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = DayOfWeek.class)
    private Set<DayOfWeek> workingDays = new HashSet<>();

    @OneToMany
    private List<Employee> employees;

    public Provider() {
    }

    public Provider(Long id, String name, String webSite, String businessDomain, String phone,
                    LocalTime startTime, LocalTime endTime, Set<DayOfWeek> workingDays ) {
        this.id = id;
        this.name = name;
        this.webSite = webSite;
        this.businessDomain = businessDomain;
        this.phone = phone;
        this.startTime = startTime;
        this.endTime = endTime;
        this.workingDays = workingDays;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getBusinessDomain() {
        return businessDomain;
    }

    public void setBusinessDomain(String businessDomain) {
        this.businessDomain = businessDomain;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Set<DayOfWeek> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(Set<DayOfWeek> workingDays) {
        this.workingDays = workingDays;
    }
}

