package com.internship.rushhour.infrastructure.exceptions;

public class EntityMissingException  extends RuntimeException {

    public EntityMissingException (String msg) {super(msg);}

}
