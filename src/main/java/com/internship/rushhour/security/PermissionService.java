package com.internship.rushhour.security;

import com.internship.rushhour.domain.account.repository.AccountRepository;
import com.internship.rushhour.domain.client.models.ClientGetDTO;
import com.internship.rushhour.domain.client.service.ClientService;
import com.internship.rushhour.domain.employee.entity.Employee;
import com.internship.rushhour.domain.employee.models.EmployeeCreateDTO;
import com.internship.rushhour.domain.employee.models.EmployeeGetDTO;
import com.internship.rushhour.domain.employee.service.EmployeeService;
import com.internship.rushhour.domain.employee.repository.EmployeeRepository;
import com.internship.rushhour.domain.provider.service.ProviderService;
import com.internship.rushhour.security.models.ContextUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("permissionService")
public class PermissionService {

    private final EmployeeService employeeService;
    private final ClientService clientService;

    @Autowired
    public PermissionService(ProviderService providerService,
                             EmployeeService employeeService, EmployeeRepository employeeRepository, AccountRepository accountRepository, ClientService clientService) {
        this.employeeService = employeeService;
        this.clientService = clientService;
    }

    private static UserDetails getPrincipal() {
        var securityContext = SecurityContextHolder.getContext();
        return (UserDetails) securityContext.getAuthentication().getPrincipal();
    }

    public boolean canProviderAdminManageProvider(Long id) {
        var userLogin = (ContextUser) getPrincipal();
        Employee providerAdmin = employeeService.findByAccountId(userLogin.getId());
        return providerAdmin.getProvider().getId().equals(id);
    }

    public boolean canProviderAdminCreateEmployee(EmployeeCreateDTO employee) {
        var userLogin = (ContextUser) getPrincipal();
        Employee providerAdmin = employeeService.findByAccountId(userLogin.getId());
        return providerAdmin.getProvider().getId().equals(employee.providerId());
    }

    public boolean canProviderAdminManageEmployee(Long id) {
        var userLogin = (ContextUser) getPrincipal();

        Employee providerAdmin = employeeService.findByAccountId(userLogin.getId());
        if (providerAdmin.getId().equals(id)) {
            return true;
        }
        List<Employee> employeeList = employeeService.findByProviderId(providerAdmin.getProvider().getId());
        for (Employee employee : employeeList) {
            if (employee.getId() == id) {
                return true;
            }
        }
        return false;
    }

    public boolean canEmployeeAccess(Long id) {
        var userLogin = (ContextUser) getPrincipal();
        EmployeeGetDTO employee = employeeService.getById(id);
        return employee.accountGetDTO().id().equals(userLogin.getId());
    }

    public boolean canClientAccess(Long id) {
        var userLogin = (ContextUser) getPrincipal();
        ClientGetDTO client = clientService.getById(id);
        return client.accountGetDTO().id().equals(userLogin.getId());
    }
}
