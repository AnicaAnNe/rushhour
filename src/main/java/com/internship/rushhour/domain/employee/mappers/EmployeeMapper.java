package com.internship.rushhour.domain.employee.mappers;

import com.internship.rushhour.domain.account.mappers.AccountMapper;
import com.internship.rushhour.domain.account.service.AccountService;
import com.internship.rushhour.domain.employee.entity.Employee;
import com.internship.rushhour.domain.employee.models.EmployeeCreateDTO;
import com.internship.rushhour.domain.employee.models.EmployeeGetDTO;
import com.internship.rushhour.domain.employee.models.ProviderAdminRequestDto;
import com.internship.rushhour.domain.provider.service.ProviderService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {ProviderService.class , AccountService.class, AccountMapper.class} )
public interface EmployeeMapper {

    @Mapping(source = "providerId", target = "provider")
    @Mapping(source = "account",target = "account", qualifiedByName =  "employeeAccount")
    Employee employeeCreateDTOtoEmployee(EmployeeCreateDTO employeeCreateDTO);

    @Mapping(source = "provider.id", target = "providerGetDTO")
    @Mapping(source = "account.id", target = "accountGetDTO")
    EmployeeGetDTO employeeToEmployeeGetDTO(Employee employee);

    @Mapping(source = "account",target = "account", qualifiedByName =  "updateAccount")
    void update(@MappingTarget Employee employee, EmployeeCreateDTO employeeCreateDTO);

    @Mapping(source = "account",target = "account", qualifiedByName =  "ProviderAdminAccount")
    Employee providerAdminRequestDtoProviderAdmin(ProviderAdminRequestDto providerAdminRequestDto);
}
