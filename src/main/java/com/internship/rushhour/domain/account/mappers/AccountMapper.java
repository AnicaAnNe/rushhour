package com.internship.rushhour.domain.account.mappers;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.account.models.AccountCreateDTO;
import com.internship.rushhour.domain.account.models.AccountGetDTO;
import com.internship.rushhour.domain.role.service.RoleService;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

@Mapper(componentModel = "spring", uses = {RoleService.class, PasswordEncoderMapper.class})
@Named("AccountMapper")
public interface AccountMapper {
    AccountGetDTO accountToAccountGetDTO(Account account);

    @Mapping(target = "email", expression = "java(checkEmail(account.getEmail(), acc.email()))")
    @Mapping(target = "password", qualifiedByName = "encode")
    @Named("updateAccount")
    void update(@MappingTarget Account account, AccountCreateDTO acc);

    @Mapping(constant = "ROLE_CLIENT", target = "role")
    @Mapping(target = "password", qualifiedByName = "encode")
    @Named("clientAccount")
    Account accountCreateDTOtoAccountClient(AccountCreateDTO accountCreateDTO);

    @Mapping(constant = "ROLE_EMPLOYEE", target = "role")
    @Mapping(target = "password", qualifiedByName = "encode")
    @Named("employeeAccount")
    Account accountCreateDTOtoAccountEmployee(AccountCreateDTO accountCreateDTO);

    @Mapping(constant = "ROLE_PROVIDER_ADMIN", target = "role")
    @Mapping(target = "password", qualifiedByName = "encode")
    @Named("ProviderAdminAccount")
    Account accountCreateDTOtoAccountProviderAdmin(AccountCreateDTO accountCreateDTO);

    default String checkEmail(String email, String newEmail) {
        if (!(email.equals(newEmail)))
            throw new ConflictException("Can not update email");
        return email;
    }
}
