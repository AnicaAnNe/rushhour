
INSERT INTO `accounts` (email, full_name, password, role_id) VALUES
('admin', 'admin@gmail.com', '$2a$12$e8k4YvhcTVsVaPxUIqSITucnCWwV8dX6EEHTWIexNX0L3GjzqKqzC', 1);

DELETE FROM roles WHERE id=2;

INSERT INTO roles (id, name) VALUES
(2, 'ROLE_PROVIDER_ADMIN');

