package com.internship.rushhour.domain.employee.models;

import com.internship.rushhour.domain.account.models.AccountGetDTO;
import com.internship.rushhour.domain.provider.models.ProviderGetDTO;

import java.time.LocalDate;

public record EmployeeGetDTO(String title, String phone, double ratePerHour, LocalDate hireDate,
                             ProviderGetDTO providerGetDTO, AccountGetDTO accountGetDTO) {}
