package com.internship.rushhour.domain.account.models;
import javax.validation.constraints.*;

public record AccountCreateDTO(
        @NotBlank(message = "Email should not be blank")
        @Email(message = "Email is not valid")
        String email,

        @NotBlank(message = "Full name should not be blank")
        @Size(min = 3, message = "Length of full name must be at least 3 chars long")
        @Pattern(regexp = "^[a-zA-Z](?:[ '.\\-a-zA-Z]*[a-zA-Z])?$",
        message = "only letters, hyphens, and apostrophes are allowed")
        String fullName,

        @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
        message = "at least 8 characters, one uppercase, one lowercase, one digit, and a special symbol")
        @NotBlank(message = "Password should not be blank")
        String password
) {}


