package com.internship.rushhour.domain.client.models;

import com.internship.rushhour.domain.account.models.AccountGetDTO;

public record ClientGetDTO(Long id, String phone, String address, AccountGetDTO accountGetDTO) {
}
