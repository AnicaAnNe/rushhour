CREATE TABLE IF NOT EXISTS clients(
   id int NOT NULL AUTO_INCREMENT,
   phone varchar(255) NOT NULL,
   address varchar(255) NOT NULL,
   account_id int NOT NULL,
   PRIMARY KEY(id),
   FOREIGN KEY (account_id) REFERENCES accounts(id)
);


