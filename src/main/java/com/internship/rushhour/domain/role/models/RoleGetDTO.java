package com.internship.rushhour.domain.role.models;

public record RoleGetDTO(Long id, String name) {
}
