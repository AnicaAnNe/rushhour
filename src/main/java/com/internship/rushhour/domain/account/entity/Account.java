package com.internship.rushhour.domain.account.entity;
import com.internship.rushhour.domain.employee.entity.Employee;
import com.internship.rushhour.domain.role.entity.Role;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity(name = "accounts")
public class  Account{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Email should not be blank")
    @Email(message = "Email is not valid")
    @Column(unique = true)
    private String email;

    @NotBlank(message = "Full name should not be blank")
    @Size(min = 3, message = "Length of full name must be at least 3 chars long")
    @Pattern(regexp = "^[a-zA-Z](?:[ '.\\-a-zA-Z]*[a-zA-Z])?$",
            message = "only letters, hyphens, and apostrophes are allowed")
    @Column(name="full_name")
    private String fullName;

    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            message = "at least 8 characters, one uppercase, one lowercase, one digit, and a special symbol")
    @NotBlank(message = "Password should not be blank")
    @Column
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToOne(mappedBy = "account")
    private Employee employee;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Account(){}

    public Account(Long id, String email, String fullName, String password) {
        this.id = id;
        this.email = email;
        this.fullName = fullName;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
