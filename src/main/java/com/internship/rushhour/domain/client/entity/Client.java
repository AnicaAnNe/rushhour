package com.internship.rushhour.domain.client.entity;


import com.internship.rushhour.domain.account.entity.Account;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity(name = "clients")
public class Client
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Phone should not be blank")
    @Pattern(regexp = "^[+0-9]+$" , message = "Numbers only , optionally can start with +")
    private String phone;

    @NotBlank(message = "Address should not be blank")
    @Size(min = 3, message = "Length of a address must be at least 3 chars long")
    private String address;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "account_id")
    private Account account;

    public Client() {
    }

    public Client(Long id, String phone, String address, Account account) {
        this.id = id;
        this.phone = phone;
        this.address = address;
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
