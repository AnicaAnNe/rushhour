CREATE TABLE IF NOT EXISTS providers(
   id int NOT NULL AUTO_INCREMENT,
   name varchar(255) NOT NULL UNIQUE,
   web_site varchar(255) NOT NULL,
   business_domain varchar(255) NOT NULL UNIQUE,
   phone varchar(255) NOT NULL,
   start_time TIME NOT NULL,
   end_time TIME NOT NULL,
   PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS providers_working_days(
     providers_id int NOT NULL,
     working_days varchar(255) NOT NULL,
     FOREIGN KEY (providers_id) REFERENCES providers(id)
);


