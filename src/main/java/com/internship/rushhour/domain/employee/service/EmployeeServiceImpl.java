package com.internship.rushhour.domain.employee.service;

import com.internship.rushhour.domain.account.service.AccountService;
import com.internship.rushhour.domain.employee.entity.Employee;
import com.internship.rushhour.domain.employee.mappers.EmployeeMapper;
import com.internship.rushhour.domain.employee.models.EmployeeCreateDTO;
import com.internship.rushhour.domain.employee.models.EmployeeGetDTO;
import com.internship.rushhour.domain.employee.models.ProviderAdminRequestDto;
import com.internship.rushhour.domain.employee.repository.EmployeeRepository;
import com.internship.rushhour.domain.provider.entity.Provider;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    private final AccountService accountService;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper, AccountService accountService) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
        this.accountService = accountService;
    }

    @Override
    public EmployeeGetDTO create(EmployeeCreateDTO employeeCreateDTO) {
        accountService.checkIfEmailExists(employeeCreateDTO.account().email());
        Employee employee = employeeMapper.employeeCreateDTOtoEmployee(employeeCreateDTO);
        return employeeMapper.employeeToEmployeeGetDTO(employeeRepository.save(employee));
    }

    @Override
    public EmployeeGetDTO getById(Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() ->
                        new EntityMissingException("That employee does not exists with id " + id));
        return employeeMapper.employeeToEmployeeGetDTO(employee);
    }

    @Override
    public Page<EmployeeGetDTO> get(Pageable pageable) {
        return employeeRepository.findAll(pageable).map(employeeMapper::employeeToEmployeeGetDTO);
    }


    @Override
    public void delete(Long id) {
        if (!employeeRepository.existsById(id)) {
            throw new EntityMissingException("That employee does not exists with id " + id);
        }
        employeeRepository.deleteById(id);
    }

    @Override
    public EmployeeGetDTO update(EmployeeCreateDTO employeeCreateDTO, Long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() ->
                new EntityMissingException("That employee does not exists with id " + id));
        if (!employee.getProvider().getId().equals(employeeCreateDTO.providerId())) {
            throw new ConflictException("Cannot update Provider");
        }
        employeeMapper.update(employee, employeeCreateDTO);
        return employeeMapper.employeeToEmployeeGetDTO(employeeRepository.save(employee));
    }

    @Override
    public void createProviderAdmin(ProviderAdminRequestDto providerAdminRequestDto, Provider provider) {
        accountService.checkIfEmailExists(providerAdminRequestDto.account().email());
        Employee employee = employeeMapper.providerAdminRequestDtoProviderAdmin(providerAdminRequestDto);
        employee.setProvider(provider);
        employeeRepository.save(employee);
    }

    @Override
    public Employee findByAccountId(Long id) {
        return employeeRepository.findByAccountId(id).orElseThrow(
                () -> new EntityMissingException("That Employee doesn't exist"));
    }

    @Override
    public List<Employee> findByProviderId(Long id) {
        return employeeRepository.findByProviderId(id);
    }

}
