package com.internship.rushhour.infrastructure.exceptions;

public class ConflictException extends RuntimeException {

    public ConflictException(String msg) {
        super(msg);
    }
}
