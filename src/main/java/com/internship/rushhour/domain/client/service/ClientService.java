package com.internship.rushhour.domain.client.service;

import com.internship.rushhour.domain.client.models.ClientCreateDTO;
import com.internship.rushhour.domain.client.models.ClientGetDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClientService {

    ClientGetDTO create(ClientCreateDTO clientCreatedDTO);

    ClientGetDTO getById(Long id);

    Page<ClientGetDTO> get(Pageable pageable);

    void delete(Long id);

    ClientGetDTO update(ClientCreateDTO clientCreatedDTO, Long id);

}
