package com.internship.rushhour.domain.employee.repository;

import com.internship.rushhour.domain.employee.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findByAccountId(Long accountId);

    List<Employee> findByProviderId(Long id);


}
