package com.internship.rushhour.domain.role.service;
import com.internship.rushhour.domain.role.entity.Role;
import com.internship.rushhour.domain.role.repository.RoleRepository;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService{

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository)
    {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role getByName(String name)
    {
        return roleRepository.findByName(name)
                .orElseThrow(() -> new EntityMissingException("That role  with name" + name + " does not exists" ));
    }
}
