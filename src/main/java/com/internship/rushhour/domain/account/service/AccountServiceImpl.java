package com.internship.rushhour.domain.account.service;

import com.internship.rushhour.domain.account.entity.Account;
import com.internship.rushhour.domain.account.mappers.AccountMapper;
import com.internship.rushhour.domain.account.models.AccountGetDTO;
import com.internship.rushhour.domain.account.repository.AccountRepository;
import com.internship.rushhour.infrastructure.exceptions.ConflictException;
import com.internship.rushhour.infrastructure.exceptions.EntityMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final AccountMapper accountMapper;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
    }


    @Override
    public AccountGetDTO getById(Long id) {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new EntityMissingException("That account does not exists with id " + id));

        return accountMapper.accountToAccountGetDTO(account);
    }

    @Override
    public void checkIfEmailExists(String email) {

        if (accountRepository.existsByEmail(email)) {
            throw new ConflictException("That email is already exists , try with another");
        }

    }

}
